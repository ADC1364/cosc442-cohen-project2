/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author aviel
 *
 */
public class VendingMachineTest {
	
	VendingMachine testMach;
	VendingMachineItem testItem1, testItem2, testItem3;
	double amount, badAmount, lowAmount;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
		testMach = new VendingMachine();
		
		testItem1 = new VendingMachineItem("chips", 1.50);
		testItem2 = new VendingMachineItem("candy bar", .01);
		testItem3 = new VendingMachineItem("trail mix", 1.05);
		
		amount = 2.5;
		badAmount = - 2.5;
		lowAmount = .5;
	}
	
	
	// this test evaluates if an exception is thrown when an item is added to a slot that is already filled.
	@Test
	public void testAddItemSlotFilled() {
		testMach.addItem(testItem1, "A");
		
		try {
			   testMach.addItem(testItem1, "A" );
			   fail();
			} catch (Exception excessItem) {
			   // expected
			   // could also check for message of exception, etc
				assertTrue(excessItem.getMessage().contentEquals("Slot A already occupied"));
			}
		
	}
	
	//this test evaluates if an exception is thrown when an invalid code is used to add an item.
	@Test
	public void testAddItemWrongCode() {
		
		try {
			   testMach.addItem(testItem2, "a" );
			   fail();
			} catch (Exception wrongCode) {
			   // expected
			   // could also check for message of exception, etc
				assertTrue(wrongCode.getMessage().contentEquals("Invalid code for vending machine item"));
			}
		
		
	}
	
	//this test evaluates if an item is added when the addItem() function is used in a valid way.
	@Test
	public void testAddItem() {
		
		testMach.addItem(testItem2, "B");
		assertNotNull(testMach.getItem("B"));
		
		
		
	}
	
	
	
	
	// this test adds an item and attempts to remove it, with the removeItem() function. 
	//When the slot is empty the getItem() function should result in null, the slot is empty.
	@Test
	public void testRemoveItem() {
		testMach.addItem(testItem1, "A");
		testMach.removeItem("A");
		
		assertNull(testMach.getItem("A"));
		
		
		
	}
	
	
	//this test evaluates if an exception is thrown when the removeItem() function is used on a slot that is already empty.
	@Test
	public void TestRemoveEmpty() {
		
		try {
			   testMach.removeItem( "A" );
			   fail();
			} catch (Exception alreadyEmpty) {
			   // expected
			   // could also check for message of exception, etc
				assertTrue(alreadyEmpty.getMessage().contentEquals("Slot A is empty -- cannot remove item"));
			}
		
	}
	
	
	
	//this test evaluates if an exception is thrown when an invalid code is used to remove an item.
	@Test
	public void testRemoveItemWrongCode() {
		testMach.addItem(testItem3, "C");
		try {
				testMach.removeItem( "c" );
				   fail();
			} catch (Exception wrongCode) {
				   // expected
				   // could also check for message of exception, etc
					assertTrue(wrongCode.getMessage().contentEquals("Invalid code for vending machine item"));
			}
		}
		
	// this test ensures that the starting balance of the vending machine is zero, and the getBalance() function obtains the current balance.
	@Test
	public void testGetBalanceZero(){
			
		assertEquals(0,testMach.getBalance(), 0.0001);
	}
		
		
		
	
	//this test evaluates if, when money is inserted, the balance changes.
	@Test
	public void testInsertMoney() {
		
		testMach.insertMoney(amount);
		assertEquals(2.5, testMach.getBalance(), .0001);
	}
	
	//this test evaluates if, when an amount < 0 is inserted, an exception is thrown.
	@Test
	public void testInsertBadAmount() {
		
		try {
			   testMach.insertMoney(badAmount);;
			   fail();
			} catch (Exception invalidAmount) {
			   // expected
			   // could also check for message of exception, etc
				assertTrue(invalidAmount.getMessage().contentEquals("Invalid amount.  Amount must be > 0"));
			}
	}
	
	

	
	//this test checks that an item can't be purchased if the balance < price of the item
	@Test
	public void testLowBalancePurch() {
		testMach.addItem(testItem1, "D");
		testMach.insertMoney(lowAmount);
		
		assertFalse(testMach.makePurchase("D"));
		
		
	}
	
	//this test checks that an item can't be purchased if it is not in stock.
	@Test
	public void testEmptySlotPurch() {
		testMach.insertMoney(amount);
		
		assertFalse(testMach.makePurchase("A"));
	}
	
	//this test checks that a purchase can be made if the the slot has an item and the balance >= price
		@Test
		public void validPurchase() {
			
			testMach.insertMoney(amount);
			testMach.addItem(testItem3, "B");
			
			assertTrue(testMach.makePurchase("B"));
		}
		
	
	//this test checks that change is returned following a purchase.
	@Test
	public void testReturnChange() {
		
		assertEquals(0, testMach.returnChange(), 0.0001);
	}
	

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		testMach = null;
		testItem1 = null;
		testItem2 = null;
		amount = 0;
		badAmount = 0;
		lowAmount = 0;
	}

}
