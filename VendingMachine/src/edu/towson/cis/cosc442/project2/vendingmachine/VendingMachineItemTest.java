/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author aviel
 *
 */
public class VendingMachineItemTest {
	
	/** Declaring necessary test objects for {@link VendingMachineItem} */
	VendingMachineItem item1,item2;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		 item1 = null;
		 item2 = new VendingMachineItem("chips", 1.50);
		 
	}

	// this test ensures that the constructor is working
	@Test
	public void testConstructor(){
		
		item1 = new VendingMachineItem("candy bar", 1.00);
		
		assertNotNull(item1);
		
	}
	
	// this test is testing the exception that is thrown with the constructor if the price entered for an item is below zero.
	@Test
	public void testInvalidPriceConstructor() {
		
		try {
					   VendingMachineItem wrongItem = new VendingMachineItem("candy bar", 0); // an item in a vending machine would never be free.
					   fail();
					} catch (Exception lowprice) {
					   // expected
					   // could also check for message of exception, etc
						assertTrue(lowprice.getMessage().contentEquals("Price cannot be less than or equal to zero"));
					}
		
	}
	
	//this test is testing the getName() method of the VendingMachineItem class
	@Test
	public void testGetName() {
		
		assertTrue("chips".contentEquals(item2.getName()));
		
		
	}
	
	//this test is testing the getPrice() method 
	@Test
	public void testGetPrice() {
		
		assertEquals(1.50, item2.getPrice(),.0001);
	}
	
	
	
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		item1 = null;
		item2 = null; 
	}

}
