package edu.towson.cis.cosc442.project2.rectangle;

/**
 * The Point Class.
 */
public class Point {
	
	/** x and y coordinates. */
	public Double x, y;
	
	/**
	 * Instantiates a new point.
	 *
	 * @param num1 the x
	 * @param num2 the y
	 */
	// I changed the parameters names to see if a bug exists more easily.
	Point(Double num1, Double num2) {
		this.x = num1; // There was a bug on this line, should have been this.x = x not y. That was tricky.
		this.y = num2;
	}
}
